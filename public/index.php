<?php

require __DIR__ . '/../vendor/autoload.php';

$query_string = $_SERVER['QUERY_STRING'];
$request_method = $_SERVER['REQUEST_METHOD'];
$redirect_url = $_SERVER['REDIRECT_URL'];

//dump($query_string);
//dump($request_method);
//dump($redirect_url);

if ( $request_method == 'GET' ) {

    $db = \App\Core\DB::getPdo();

    switch ($redirect_url) {
        case false:
        case 'index':
            $el = new \App\Repository\DataRepository($db);

            $new = new \App\Entity\Data();
            $new->setParentId(1);
            $new->setPosition(1);
            $new->setPath(1.2);
            $new->setLevel(2);

            $newId = $el->save($new);
            echo 'New element ID = ' . $newId;
            dd( $el->getById($newId) );
            break;
        case '/migration':
            $migration = new \Migration\Migration($db);
            $migration->up();
            echo 'Migration up';
            break;
        default:
            exit('404 page not found');
    }
}
?>
<!---->
<!--<!doctype html>-->
<!--<html lang="en">-->
<!---->
<!--  <head>-->
<!--    <meta charset="utf-8">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
<!--	<title>Test</title>-->
<!--  </head>-->
<!---->
<!--  <body>-->
<!--	<header>-->
<!--      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">-->
<!--        <a class="navbar-brand" href="#">Test</a>-->
<!--      </nav>-->
<!--    </header>-->
<!---->
<!--   <main role="main" class="container">-->
<!---->
<!--    </main>-->
<!---->
<!--    <footer class="footer">-->
<!--      <div class="container">-->
<!--        <span class="text-muted">@2020</span>-->
<!--      </div>-->
<!--    </footer>-->
<!--  </body>-->
<!---->
<!--</html>-->