<?php

declare(strict_types=1);

namespace App\Entity;

class Data
{
    private static $table = 'data';

    private $id;
    private $parent_id;
    private $position;
    private $path;
    private $level;

    public static function getTable()
    {
        return static::$table;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParentId(): int
    {
        return $this->parent_id;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setParentId(int $data)
    {
        $this->parent_id = $data;
    }

    public function setPosition(int $data)
    {
        $this->position = $data;
    }

    public function setPath(string $data)
    {
        $this->path = $data;
    }

    public function setLevel(int $data)
    {
        $this->level = $data;
    }

}