<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Data;

class DataRepository
{
    private $pdo;
    private $table;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->table = Data::getTable();
    }

    public function getById(int $id): ?Data
    {
        $sql = sprintf('SELECT * FROM %s WHERE id = :id', $this->table);

        $select = $this->pdo->prepare($sql);
        $select->execute([':id' => $id]);
        $result = $select->fetchObject(Data::class);

        if ($result === false) {
            return null;
        }

        return $result;
    }

    public function save(Data $data): int
    {
        $sql = sprintf(
            "INSERT INTO %s (parent_id, position, path, level) 
                    VALUE (:parent_id, :position, :path, :level)",
            $this->table
        );

        $stmt = $insert = $this->pdo->prepare($sql);
        $stmt->bindParam(':parent_id', $data->getParentId());
        $stmt->bindParam(':position', $data->getPosition());
        $stmt->bindParam(':path', $data->getPath());
        $stmt->bindParam(':level', $data->getLevel());

        if ( $stmt->execute() === false ) {
            echo $this->getError($stmt->errorInfo());
            die;
        }

        return (int)$this->pdo->lastInsertId();
    }

    private function getError(array $error): string
    {
        return $error[2] ?? '';

    }
}