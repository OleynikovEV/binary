<?php

declare(strict_types=1);

namespace App\Core;

class DbConfig
{
    private $driver;
    private $host;
    private $port;
    private $db_name;
    private $user;
    private $password;

    public function __construct(string $driver, string $host, int $port, string $db_name, string $user, string $password)
    {
        $this->driver = $driver;
        $this->host = $host;
        $this->port = $port;
        $this->db_name = $db_name;
        $this->user = $user;
        $this->password = $password;
    }

    public function getDriver(): string
    {
        return $this->driver;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): int
    {
        return $this->port;
    }

    public function getDbName(): string
    {
        return $this->db_name;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}