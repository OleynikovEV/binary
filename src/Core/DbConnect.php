<?php

declare(strict_types=1);

namespace App\Core;

use PDOException;
use PDO;

class DbConnect
{
    private $config;
    private $dsn;
    private $pdo;

    public function __construct(DbConfig $config)
    {
        $this->config = $config;

        $this->dsn = sprintf(
            '%s:host=%s;port=%s;dbname=%s;charset=utf8',
            $config->getDriver(),
            $config->getHost(),
            $config->getPort(),
            $config->getDbName(),
        );

        try {
            $this->pdo = new PDO($this->dsn, $config->getUser(), $config->getPassword());
        } catch (PDOException $e) {
            echo $e->getMessage();
            die;
        }
    }

    public function getConnect(): PDO
    {
        return $this->pdo;
    }
}