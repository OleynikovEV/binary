<?php

declare(strict_types=1);

namespace App\Core;

class DB
{
    private static $pdo;

    private function __construct()
    {
        $config = include __DIR__ . '/../../config/db.php';
        $DbConfig = new DbConfig(
            $config['db_driver'],
            $config['db_host'],
            $config['db_port'],
            $config['db_name'],
            $config['db_user'],
            $config['db_password']
        );

        $DbConnect = new DbConnect($DbConfig);
        self::$pdo = $DbConnect->getConnect();
    }

    public static function getPdo(): \PDO
    {
        if (self::$pdo) {
            return self::$pdo;
        }

        $instense = new static;
        return $instense::$pdo;
    }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }
}