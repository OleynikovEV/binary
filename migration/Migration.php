<?php

declare(strict_types=1);

namespace Migration;

class Migration
{
    private $pdo;
    private $table;
    private $columns;
    private $indices;
    private $data;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;

        $this->table = 'data';

        $this->columns = [
            'id' => 'INT(11) AUTO_INCREMENT PRIMARY KEY',
            'parent_id' => 'INT(11) UNSIGNED NOT NULL', // идентификатор родителя
            'position' => 'TINYINT UNSIGNED NOT NULL', // позиция ячейки относительно родителя (1, 2)
            'path' => 'VARCHAR(12288) NOT NULL', // путь ячейки
            'level' => 'INT(11) UNSIGNED NOT NULL', // уровень бинара
        ];

        $this->indices = [
            'parent_id' => 'data_parent_id',
        ];

        $this->data = [
            [
                'id' => 1,
                'parent_id' => 0,
                'position' => 0,
                'path' => 1,
                'level' => 1,
            ],
        ];
    }

    public function up()
    {
        $this->createTable();
        $this->createIndices();
        $this->insert();
    }

    private function createTable(): bool
    {
        $sql = 'CREATE TABLE IF NOT EXISTS %s (%s);';

        $columns = implode(', ',
            array_map(function($column, $item) {
                return $column . ' ' . $item;
            }, array_keys($this->columns), $this->columns)
        );

        $createIfNotExist = sprintf($sql, $this->table, $columns);

        $result = $this->pdo->exec($createIfNotExist);

        if ($result === false) {
            echo $this->getError($this->pdo->errorInfo());
            die;
        }

        return true;
    }

    private function createIndices(): bool
    {
        $createIndex = '';

        foreach ($this->indices as $column => $index) {
            $createIndex .= sprintf('ALTER TABLE %s ADD INDEX %s (%s);',
                $this->table,
                $index,
                $column
            );
        }

        $result = $this->pdo->exec($createIndex);

        if ($result === false) {
            echo $this->getError($this->pdo->errorInfo());
            die;
        }

        return true;
    }

    private function insert()
    {
        $sql = 'INSERT INTO %s (%s) VALUE(%s)';
        $fields = implode(',', array_keys($this->columns));
        $vars = implode(',', array_map(function($field) {
                return ':' . $field;
            }, array_keys($this->columns))
        );

        $sql = sprintf($sql, $this->table, $fields, $vars);
        $stmt = $this->pdo->prepare($sql);

        foreach ($this->data as $item) {
            if (! $stmt->execute($item) ) {
                echo $this->getError($stmt->errorInfo());
                die;
            }
        }
    }

    private function getError(array $error): string
    {
        return $error[2] ?? '';

    }
}